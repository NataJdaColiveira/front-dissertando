const routes = [
  {
    path: '/',
    component: () => import('pages/Dashboard.vue'),
    meta: {
      loggedIn: true,
    },
  },

  {
    path: '/login',
    component: () => import('pages/Login.vue'),
    meta: {
      loggedIn: false,
    },
  },

  {
    path: '/registro-usuario',
    component: () => import('src/pages/User/RegisterUser.vue'),
    meta: {
      loggedIn: false,
    },
  },

  {
    path: '/atualizar-usuario',
    component: () => import('src/pages/User/UpdateUser.vue'),
    meta: {
      loggedIn: true,
    },
  },

  {
    path: '/criar-redacao',
    component: () => import('src/pages/Redaction/CreateRedaction.vue'),
    meta: {
      loggedIn: true,
    },
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/404.vue'),
  });
}

export default routes;
