import { LocalStorage } from 'quasar';

export default ({ router, store, Vue }) => {
  router.beforeEach((to, from, next) => {
    const requiresAuthentication = to.meta.loggedIn;
    const userToken = LocalStorage.getItem('token');

    if (requiresAuthentication) {
      if (userToken) {
        next();
      } else {
        next('/login');
      }
    } else if (userToken) {
      next('/');
    } else {
      next();
    }
  });
};
