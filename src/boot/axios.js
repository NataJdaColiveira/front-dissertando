import Vue from 'vue';
import axios from 'axios';
import { LocalStorage } from 'quasar';

Vue.prototype.$axios = axios;

const AUTHORIZATION_HEADER = 'Authorization';
axios.defaults.timeout = 30000;
axios.defaults.baseURL = 'http://localhost:1323';

axios.interceptors.request.use(
  config => {
    if (LocalStorage.getItem('token')) {
      config.headers.common[AUTHORIZATION_HEADER] = LocalStorage.getItem(
        'token'
      );
    }

    return config;
  },
  error => Promise.reject(error)
);
