import Axios from 'axios';
import { LocalStorage } from 'quasar';

export const register = async ({ commit }, credentials) => {
  await Axios.post('/user', credentials);
};

export const login = async ({ commit }, credentials) => {
  const { data } = await Axios.post('/login', credentials);
  LocalStorage.set('token', data.token);
  LocalStorage.set('user', data.user);

  commit('updateUserData', data);
};

export const logout = async ({ commit }) => {
  LocalStorage.set('token', '');
  LocalStorage.set('user', { name: '', email: '', role: 1 });

  commit('updateUserData', {
    token: '',
    user: { name: '', email: '', role: 0 },
  });
};
