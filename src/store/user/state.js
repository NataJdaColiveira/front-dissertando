import { LocalStorage } from 'quasar';

export default {
  token: LocalStorage.getItem('token') || ' ',
  userData: LocalStorage.getItem('user') || { nome: '', email: '', role: 1 },
};
